#%%#########################################################################
#   Import libraries 
############################################################################
import numpy as np 
import Functions as fn
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
from sklearn.metrics import confusion_matrix
import sys
import graphviz
from pathlib import Path

#%%#########################################################################
#   Functions 
############################################################################
Estimator = lambda mln,cr: DecisionTreeClassifier(
        random_state=0, 
        max_leaf_nodes=mln, 
        criterion=cr, 
        class_weight='balanced'
    )
HealthFault = lambda y: np.array( [1 if i != 0 else 0 for i in y] ) 

#%%#########################################################################
#   Load List Data  and Initial Data 
############################################################################
print('Estimator are:\n    dt = DecisionTree or First Step of CDT\
    \n    cdt = Second Step of CDT')
est = str(input('Select Estimator:'));    print(' ')
CreateXy = str(input(
    "Create Matrix X and  target vector y:\n    \'yes\' or \'not\'?  ")) 
print(' ')
CreateMLN = str(input(
    "Calculate data of Max Leaf Nodes: \n    \'yes\' or \'not\'?  "))
print(' ')
cr = str(input("Criterion: \n    \'gini\' or \'entropy\'?  "))
print(' ')
#___________________________________________________________________________
folder = '1_Results/' + est + '/'
List = np.load(folder + '02_SelectedSamples.npy')   # List of Data
I = np.load(folder + '02_FaultIndex.npy').item()    # Index by fault in data
folderload = '0_Data/2_Samples/'                    # Where Load Global Data

#___________________________________________________________________________
SaveData = folder
SaveResult = SaveData + cr + '/'
#___________________________________________________________________________
Var = ['P1','P2','DP','M','T','Tin','Tout','Pow','Eff'] # Features 
#Var = ['P1','P2','M','Tout','Eff'] # Features 
G = 6                   # Number of Group to divide the original dataset
Gr = ['X', 'Xd']; # Make with only Gradient  'Xd', only data 'X' or both ''
W = 33;                 # Window for Filter Gradient every 33 samples 1 [s]
BF = 'Bi2';             # Biphasic 'Bi2' or Monophasic 'Mo2'
F = [0,1,3,5,7] # Biphasic, FaultSel, Groups _______________________________
                #'\n    0 = Static\n    1 = Close Valve\n    2 = Open Valve\
                # \n    3 = P Inlet Decrease\n    4 = P Inlet Increase\
                # \n    5 = Viscosity Increase\
                # \n    6 = Mgas Decrease\n    7 = Mgas Increase\
                # \n    8 = RPM Increase\n    9 = RPM Decrease'
Vtr = [i for i in range(0,len(Gr)*len(Var))]
#for i in [11,13,14]: Vtr.pop(i)
MLN = [i for i in range(4,101)]                         # Max Leaf Number

#%%#########################################################################
#   Construct five sets of Data
############################################################################
if CreateXy == 'yes':
    # Initialize ___________________________________________________________
    G_Index = {}                            # Index for every Group
    for g in range(G):                      # First Value in G Groups Empty
        G_Index[g] = np.empty([0,])         # Index in every Group g
    # Construct "G" and "G_Index" for Cross Validation _____________________
    for f in F:                             # Get index for selected Faults
        F_i = I[BF][f];  l = len(F_i)       #   Index and len
        c = [int(l/G*i) for i in range(G)]  # Where Cut every group F_i
        F_split = np.split(F_i,c)           # Fault Splited in G G_Index
        for g in range(G):                  # Append Every Group of Fault
            G_Index[g] = np.r_[G_Index[g], F_split[g+1]]
    # Get X, Y in every Cross Calidation Group _____________________________
    G_Data = {}                             # Data for every Group
    for i in G_Index:                       # Construct Data based in Index
        G_Data[i] = fn.Construct(G_Index[i],folderload,List,W,Var,Gr,F)
        print('Group Constructed = %s / %s'%(len(G_Data),G))
    np.save(SaveData + '03_GroupIndex',G_Index)
    np.save(SaveData + '03_GroupData',G_Data)
else:
    G_Index = np.load(SaveData + '03_GroupIndex.npy').item()
    G_Data = np.load(SaveData + '03_GroupData.npy').item()
#___________________________________________________________________________
feature_names = [G_Data[0]['feature_names'][i] for i in Vtr]
if est == 'cdt':
    for i in G_Data:
        I_sel = np.where(G_Data[i]['target']!=0)[0]
        G_Data[i]['target'] = G_Data[i]['target'][I_sel]
        G_Data[i]['data'] = G_Data[i]['data'][I_sel]
    target_names = G_Data[0]['target_names'][1:]
else:
    target_names = G_Data[0]['target_names']

#%%#########################################################################
#   Determination MLN Trends
############################################################################
# For for determination of best MLN bassed on maximization accuraccy of
# worst failure detected ___________________________________________________
if CreateMLN == 'yes':
    # Initialize Dictionary for Results ____________________________________
    Results = {}                        # Initial Dictionary
    for i in ['Train', 'Val', 'Test']:  # Position in Dict Results['Train']
        Results[i] = {}                 # Sub Dict Relts['Train]['sts']
        for j in ['sts', 'cmmean', 'cmmin']:    # In every Dict
            Results[i][j] = [       # Empty List of List shape [mln,groups]
                [[]for i in range(G-1)] for i in range(len(MLN))]  
    # Train To Get Resutls and Analize _____________________________________
    Ltotal = len(MLN)*(G-1);  count = 0
    for mln in MLN:                     # Fill in MLN Dimension     0
        for g in range(G-1):            # Fill in Group Dimension   1
            X, y = fn.TrainValTest(G_Data,g,G-1)   
            tree = Estimator(mln,cr)    # Calculate Tree
            tree.fit(X['Train'][:,Vtr], y['Train']) # Train Tree
            for i in ['Train', 'Val', 'Test']:
                sts, cmmean, cmmin, _ = fn.MetricS( # Get Metrics from
                    tree,X[i][:,Vtr], y[i])         # Constructed Tree
                Results[i]['sts'][mln-min(MLN)][g].append(sts)
                Results[i]['cmmean'][mln-min(MLN)][g].append(cmmean)
                Results[i]['cmmin'][mln-min(MLN)][g].append(cmmin)
            count+=1                                # Count Percent
            sys.stdout.write("\rMLN Calculated:... {:.4f} %".\
                format(100*count/Ltotal));  sys.stdout.flush()
    # Save Results _________________________________________________________
    MLN_Trend = {'MLN':MLN, 'Train':Results['Train'], 
        'Val':Results['Val'], 'Test':Results['Test']}
    np.save(SaveResult + 'MLN_Trend',MLN_Trend)
else:
    MLN_Trend = np.load(SaveResult + 'MLN_Trend.npy').item()

#%%#########################################################################
#   Get Data for Plots
############################################################################
# Get Max leaf Nodes and Number of Estimators ______________________________
MLN = np.array(MLN_Trend["MLN"])
# Get Test Data ____________________________________________________________
if Path(SaveResult + 'Test.npy').is_file() is True:
    Test = np.load(SaveResult + 'Test.npy').item()
    print('The file Test.py was load,\n    if you wanna create new file \
        \n    delete existent file and re-run')
else:
    X, y = fn.TrainValTest(G_Data,0,G-1)        # Get Data for all groups
    X_train = np.r_[X["Val"],X["Train"]][:,Vtr] # X organized Train + Val 
    y_train = np.r_[y["Val"],y["Train"]]        # y organized Train + Val
    # Initialize Dictionary for Results ____________________________________
    print('\nCalculed of Test Data \n\n')
    Test = {'sts':[], "cmmean":[], "cmmin":[]}      # Initial 
    Test = {}                               # Dict Test['Train]['sts']
    for j in ['sts', 'cmmean', 'cmmin']:    # In every Dict
        Test[j] = [                         # Empty List of List shape [mln]
            [] for i in range(len(MLN))]  
    Ltotal = len(MLN);    count = 0
    # Train To Get Resutls and Analize _____________________________________
    for mln in MLN:                         # Fill in MLN Dimension     0
        tree = Estimator(mln,cr)    # Calculate Tree
        tree.fit(X_train, y_train)              # Train Tree
        sts, cm_mean, cm_min, cm = fn.MetricS(  # Get Metrics from
            tree,X['Test'][:,Vtr],y['Test'])    # Constructed Tree
        plt.clf()
        Test['sts'][mln-min(MLN)].append(sts)
        Test["cmmean"][mln-min(MLN)].append(cm_mean)
        Test["cmmin"][mln-min(MLN)].append(cm_min)
        count+=1                                # Count Percent
        sys.stdout.write("\rTest Data MLN Calculated:... {:.4f} %".\
            format(100*count/Ltotal));  sys.stdout.flush()
    np.save(SaveResult + 'Test.npy', Test)
# Get Validation and Training Data _________________________________________
X_Plot = {}; count = 0
for stop in ["sts", "cmmean", "cmmin"]:
    # Validation and  Train Results [mln, group, nest] = [0, 1, 2] ---------
    Train_r = np.array(MLN_Trend["Train"][stop])
    Val_r = np.array(MLN_Trend["Val"][stop])
    # For every MLN get respective best n_estimators -----------------------
    Nest_i = [np.bincount(i).argmax() for i in Val_r.argmax(2)] # Index Est.
    # Get Validation -------------------------------------------------------
    Val = Val_r.max(2).mean(1)
    Val_std = Val_r.max(2).std(1)
    # Get Train ------------------------------------------------------------
    Tra = Train_r.max(2).mean(1)
    # Save Data ------------------------------------------------------------
    X_Plot[stop] ={'Train':Tra, 'Val':Val, 'Val_std':Val_std, 'Nest_i':Nest_i}

#%%#########################################################################
#   Global Plots
############################################################################
x_s = ['Train', '', 'Validation', '', 'Test']
x_c = [(0.6,0.6,0.6), (0,0,0), (0,0,0), (0,0,0), (0.6,0.6,0.6)]
x_t = [":", "_", '--+', "_", "--o"]
for criteria in ["sts", "cmmean", "cmmin"]:
    Tra = X_Plot[criteria]['Train']
    Val = X_Plot[criteria]['Val']
    Val_std = X_Plot[criteria]['Val_std']
    Nest_i = X_Plot[criteria]['Nest_i']
    Test_c = np.array(
        [Test[criteria][i][Nest_i[i]] for i in range(0,len(Nest_i))])
    x = [Tra, Val-Val_std/2, Val, Val+Val_std/2, Test_c]
    plt.clf()
    Fig1 = fn.Plot(MLN,'Max Leaf Nodes',x,x_s,x_c,x_t,0,0,0,'Equal',0,0)
    Fig1.suptitle(criteria, fontsize=20, y=1.05);   #plt.show()
    plt.savefig(SaveResult + criteria + ".pdf")


#%%#########################################################################
#   Desicion Tree Algorithm
############################################################################
MLN = np.array(MLN_Trend["MLN"])
# Get 'mln' and 'nest' for every stop criteria: sts, cmmean, cmmin _________
for stop in ["sts", "cmmean", "cmmin"]:
    # Validation, Train and Test Results [mln, group, nest] = [0, 1, 2] ____
    Train_r = np.array(MLN_Trend["Train"][stop])    # All Training Data
    Val_r = np.array(MLN_Trend["Val"][stop])        # All Validation Data
    Val = Val_r.max(2).mean(1)                      # Get Best Validation
    Val_std = Val_r.max(2).std(1)                   # Get Validation Std
    Tra = Train_r.max(2).mean(1)                    # Get Training
    # Where are the best results in Validation Sets ________________________
    Nest_i = [np.bincount(i).argmax() for i in Val_r.argmax(2)]
    i = np.argmax(Val)                      # Where is the b
    n = Nest_i[i]                           # Where is the best validation
    # Results Validation Test ______________________________________________
    np.array(MLN_Trend["Train"]["sts"])[i,:,n].mean()
    print("\n\n|||||________Stop Criteria is: %s________|||||\n\n"%stop)
    sts_tr = np.array(MLN_Trend["Train"]["sts"]).max(2).mean(1)[i]
    sts_va = np.array(MLN_Trend["Val"]["sts"]).max(2).mean(1)[i]
    cmmean = np.array(MLN_Trend["Val"]["cmmean"]).max(2).mean(1)[i]
    cmmin = np.array(MLN_Trend["Val"]["cmmin"]).max(2).mean(1)[i]
    print("|_      Validation Test      _|")
    print("Accuracy on training set:         {:.4f}".format(sts_tr))
    print("Accuracy on validation set:       {:.4f}".format(sts_va))
    print("Mean of confusion matrix diagonal:{:.4f}".format(cmmean))
    print("Min of confusion matrix diagonal: {:.4f}".format(cmmin))
    print("MLN:                              {:.0f}".format(MLN[i]))
    # Results Best Test ____________________________________________________
    Test_array = np.array(Test[stop])
    Test_best = Test_array.max()
    ib, nb = np.where(Test_array==Test_best);   ib = ib[0]; nb = nb[0]
    # Get X and y Data _____________________________________________________
    X, y = fn.TrainValTest(G_Data,0,G-1)             #
    X_train = np.r_[X["Val"],X["Train"]][:,Vtr]
    y_train = np.r_[y["Val"],y["Train"]]        # y organized Train + Val
    # Run in Seleted and Validation Test ___________________________________
    Case = ["Selected", "Best"];    it = [i, ib];   nt = [n, nb]
    for mk in range(2):
        mln = MLN[it[mk]]
        tree = Estimator(mln,cr)
        tree.fit(X_train, y_train)
        sts_tr = tree.score(X_train, y_train)
        sts, cmmean, cmmin, cm = fn.MetricS(tree,X['Test'][:,Vtr],y['Test'])
        cm_s = cm.shape[0]
        error = np.mean([np.delete(cm[i,:],i).mean() for i in range(cm_s)])
        error_falha = np.mean([np.delete(cm[i,:],[0,i]).mean() 
            for i in range(1,cm_s)])
        print("|_      %s Test      _|"%Case[mk])
        print("Accuracy on training set:         {:.4f}".format(sts_tr))
        print("Accuracy on validation set:       {:.4f}".format(sts))
        print("Mean of confusion matrix diagonal:{:.4f}".format(cmmean))
        print("Min of confusion matrix diagonal: {:.4f}".format(cmmin))
        print("Confusion with another states:    {:.4f}".format(error))
        print("Confusion with another failures:  {:.4f}".format(error_falha))
        print("MLN:                              {:.0f}".format(mln))
        String = SaveResult + str(stop)  + '_' + Case[mk]
        np.save(String + '_Tree' ,tree)
        # Export Confusion Matrix __________________________________________
        plt.clf()
        fn.plot_confusion_matrix(cm, classes=target_names, normalize=True, 
            title='Confusion matrix')
        plt.savefig(String + '_ConfusionMatrix.pdf'); #plt.show()
        # Save Tree Figures .dot and .svg __________________________________
        if est != 'rf' and est != 'ab' and est != 'bg':
            export_graphviz(tree, out_file=String + '_Tree.dot', 
                class_names=target_names, feature_names=feature_names, 
                impurity=True, filled=True)
            graphviz.render('dot','pdf',String + '_Tree.dot')
            #graphviz.view(String + '_Tree.dot)
