The purpose of this repository is to provide the necessary data to reproduce the results presented in the work **Fault identification using a chain of decision trees in a electrical submersible pump operating in a liquid-gas flow**


1.  The work was developed in **Python**; Therefore, the following packages are necessary:


* numpy 
* random
* matplotlib
* sklearn
* sys
* graphviz
* pathlib
* math
* itertools


2. The labeling of the data is done with the file [01_labeling.py](https://gitlab.com/mauricio.bc/esp_monitoring_cdt/blob/master/01_Labeling.py), The files are created in the folder [0_Data/2_Samples](https://gitlab.com/mauricio.bc/esp_monitoring_cdt/tree/master/0_Data/2_Samples), i.e.;

`python3 01.labeling.py`

3. Training, Validation and Testing are executed in [02_DecisionTree.py](https://gitlab.com/mauricio.bc/esp_monitoring_cdt/blob/master/02_DecisionTree.py), The results are saved in folder [1_Results](https://gitlab.com/mauricio.bc/esp_monitoring_cdt/tree/master/1_Results), i.e.;

`python3 02_DecisionTree.py` \\
`dt` \\
`yes` \\
`yes` \\
`entropy`

`python3 02_DecisionTree.py`\\
`cdt`\\
`yes`\\
`yes`\\
`gini`

4. After some figures and results are showed in [03_Figures.py](https://gitlab.com/mauricio.bc/esp_monitoring_cdt/blob/master/03_Figures.py)

