#%% ########################################################################
#   Import libraries 
############################################################################
import numpy as np
import Functions as fn
import random as rd

#%% ########################################################################
#   Load Data
############################################################################
Data = np.load('1_Results/01_SelectedSamples.npy')
print("\n%s Experiments to Evaluate"%(len(Data)))
folderload = '0_Data/1_Python/'
foldersave = '0_Data/2_Samples/'

#%% ########################################################################
#   Construct States
############################################################################
New_Data = [];  k = 1                       # List of Samples 
for i in Data:                              # for i in list of Data
    print('Sample = %s/%s \n    %s'%(k,len(Data),i[0]))
    sample_a = np.load(folderload + i[0]+'.npy')    # Import Sample as Array
    print('    Before Add State Shape = %s'%format(sample_a.shape))
    sample_o = fn.Sample_lvm(sample_a)      # Sample as Object
    MB = ['Bi2' if sample_o.Mgas.max()*3600 > 0.5
        else 'Mo2']                         # Monophasic or Bhifasic
    sample_o.Construct_State()              # Construct Sample State
    s = sample_o.State                      # Sample State
    sample_a = np.c_[sample_a, s]           # Add State to Saple Array
    if s.max() != 5:                        # if fault is not Temperature
        np.save(foldersave + i[0] ,sample_a)                # Save Array
        print('    After Add State Shape = %s\n'%format(sample_a.shape))
        New_Data.append(                    # Add Sample to New List
            np.r_[i, MB, s.max(), sample_o.tch])
    else:                                   # if fault is Temperature
        i1 = np.where(sample_o.t>=sample_o.tch[0])[0][0]    # Fault Begin
        i2 = np.where(sample_o.t<=sample_o.tch[1])[0][-1]   # Fault End
        cut = np.arange(i1,i2,1025)         # Cut every 30 [s]
        stringj = i[0]                      # Name of sample performed
        for j in range(0,len(cut)-1):       # Cut Faults
            j1 = cut[j];                    # New Sample Begin
            j2 = cut[j+1] if (i2-cut[j+1])>1025 \
                else i2 + 1                 # New Sample End
            sample_aj = sample_a[j1:j2]     # New Sample Array
            tj = sample_aj[:,0]             # New time vector
            sample_aj[:,0] = tj - tj[0]     # Replace Vector Time in Array
            ij = i                          # Get list [Name, MonBif, Fault]
            ij[0] = stringj + '_' + str(j)  # Replace Name by New Sample Nam
            np.save(foldersave + ij[0]+ '.npy', sample_aj)  # Save Array
            print('    After Add State Shape = %s\n'%format(sample_aj.shape))
            tch = [tj[0], tj[-1]]           # Get New Time Begin and End
            New_Data.append(                # Add Sample to New List
                np.r_[ij, MB, s.max(), tch]) 
    k += 1                                  # New Iteration
New_Data = np.array(New_Data)               # Numpy List of Samples
np.save('1_Results/02_SelectedSamples', New_Data)   # Save List of Samples
print('New list of Samples was saved in \'02_SelectedSamples.npy\'')

#%%#########################################################################
#   Import List Data and Shuffle and Get index By Fault ####################
############################################################################
Data = np.load('1_Results/02_SelectedSamples.npy')      # Load Data
Fault_Index = {'Mo2':[], 'Bi2':[]}          # Dict. whith fault location
for MB in Fault_Index:                      # For in [Monphasic, Biphasic]
    yl = 6.0 if MB =='Mo2' else 10.0        # Failure Quantity
    Fault_kind = [str(i) for i in np.arange(0,yl,1.0)]  # Float to String
    for F in Fault_kind:                    # For in kind of Failure
        Index = fn.index_data(Data,MB,3,F,4)# Get index where Data Fault
        rd.shuffle(Index)                   # shuffle list of index
        Fault_Index[MB].append(Index)       # Alocate in Dict
np.save('1_Results/02_FaultIndex',Fault_Index)
print('Save List of Split Fault Index in \'02_FaultIndex.npy\'')
