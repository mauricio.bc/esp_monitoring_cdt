############################################################################
# Import libraries #########################################################
############################################################################
import numpy as np
import matplotlib.pyplot as plt
import math
from sklearn.metrics import confusion_matrix
import itertools
from matplotlib.offsetbox import TextArea
import sys
import matplotlib.ticker as mtick
import matplotlib.ticker as ticker

############################################################################
# Create Class from Every Sampe ############################################
############################################################################
class Sample_lvm(object):
    ########################################################################
    def __init__(self, Frame):
        # Get Variables ####################################################
        self.Frame = Frame
        self.t = Frame[:,0];        self.t_s = 't [s]'
        self.P1 = Frame[:,1]*1E5;   self.P1_s = '$P_{in}\ [Pa]$'
        self.DP1 = Frame[:,2]*1E5;  self.DP1_s = '$\Delta P_{1}\ [Pa]$'
        self.DP2 = Frame[:,3]*1E5;  self.DP2_s = '$\Delta P_{2}\ [Pa]$'
        self.DP3 = Frame[:,4]*1E5;  self.DP3_s = '$\Delta P_{3}\ [Pa]$'
        self.DP4 = Frame[:,5]*1E5;  self.DP4_s = '$\Delta P_{4}\ [Pa]$'
        self.DP5 = Frame[:,6]*1E5;  self.DP5_s = '$\Delta P_{5}\ [Pa]$'
        self.DP6 = Frame[:,7]*1E5;  self.DP6_s = '$\Delta P_{6}\ [Pa]$'
        self.DP7 = Frame[:,8]*1E5;  self.DP7_s = '$\Delta P_{7}\ [Pa]$'
        self.DP8 = Frame[:,9]*1E5;  self.DP8_s = '$\Delta P_{8}\ [Pa]$'
        self.DP9 = Frame[:,10]*1E5; self.DP9_s = '$\Delta P_{9}\ [Pa]$'
        self.DP10 = Frame[:,11]*1E5;self.DP10_s = '$\Delta P_{10}\ [Pa]$'
        self.P2 = Frame [:,12]*1E5; self.P2_s = '$P_{out}\ [Pa]$'
        self.T = Frame[:,13];       self.T_s = '$Torque\ [N-m]$'
        self.rho = Frame[:,14];     self.rho_s = '$\rho\ [kg/m^{3}]$'
        self.M = Frame[:,15]/3600;  self.M_s = '$\dot M\ [kg/s]$'
        self.Tin1 = Frame[:,16];    self.Tin1_s = '$T_{in1}\ [^oC]$'
        self.Tin2 = Frame[:,17];    self.Tin2_s = '$T_{in2}\ [^oC]$'
        self.T2 = Frame[:,18];      self.T2_s = '$T_{2}\ [^oC]$'
        self.T4 = Frame[:,19];      self.T4_s = '$T_{4}\ [^oC]$'
        self.T6 = Frame[:,20];      self.T6_s = '$T_{6}\ [^oC]$'
        self.T8 = Frame[:,21];      self.T8_s = '$T_{8}\ [^oC]$'
        self.Tout1 = Frame[:,22];   self.Tout1_s = '$T_{out1}\ [^oC]$'
        self.Tout2 = Frame[:,23];   self.Tout2_s = '$T_{out2}\ [^oC]$'
        self.Mgas = Frame[:,24]/3600;self.Mgas_s = '$\dot M_{gas}\ [kg/s]$'
        self.Choke = Frame[:,25];   self.Choke_s = 'Choke Valve [%]'
        self.Bypass = Frame[:,26];  self.Bypass_s = 'Bypass Valve [%]'
        self.Booster = Frame[:,27]; self.Booster_s = 'Booster Speed [RPM]'
        self.RPM = Frame[:,28];     self.RPM_s = 'Pump Speed [RPM]'
        self.Tset = Frame[:,29];    self.Tset_s = '$T_{set}\ [^oC]$'
        self.Pgas = Frame[:,30]*1E5;self.Pgas_s = '$P_{gas}\ [Pa]$'
        self.Vgas = Frame[:,31];    self.Vgas_s = 'Gas Valve [%]'
        # Calculate ########################################################
        Tin = (self.Tin1 + self.Tin2) * 0.5 # T in [C]
        self.Q = self.M * (1/self.rho); self.Q_s = '$Q\ [m^{3}/s]$'
        self.DP = self.P2 - self.P1;    self.DP_s = '$\Delta P\ [Pa]$'
        self.Tin = (self.Tin1 + self.Tin2) * 0.5; 
        self.Tin_s = '$T_{in}\ [^oC]$'
        self.Tout = (self.Tout1 + self.Tout2)*0.5;  
        self.Tout_s = '$T_{out}\ [^oC]$'
        self.mul = Mul(Tin);            self.mul_s = '$\mu_l [Cp]$'
        self.rhol = Rhol(Tin);          self.rhol_s = '$\rho_l kg/m^{3}]$'
        self.mug = Mug(Tin);            self.mug_s = '$\mu_g [Cp]$'
        self.rhog = Rhog(Tin,self.P1);  self.rhog_s = '$\rho_g kg/m^{3}]$'
        #self.Pow = self.M*self.DP;      self.Pow_s = '$Power [W]$'
        self.Pow = self.Q*self.DP;      self.Pow_s = '$Power [W]$'
        #self.Eff = self.Pow*(1/self.T); self.Eff_s = '$Eff [-]$'
        self.Eff = self.Pow*(1/self.T)* ((60/2/np.pi)/self.RPM)
        self.Eff_s = '$Eff [-]$'
        # State ############################################################
        if self.Frame.shape[1] == 33:
            self.State = Frame[:,32];               s = self.State.max()
            i1 = np.where(self.State==s)[0][0]
            i2 = np.where(self.State==s)[-1][0]
            self.tch = [self.t[i1], self.t[i2]];    self.ich = [i1,i2]
        else:
            msg = 'Warning: The sample has not defined \"self.State\" \
                \n    run \"self.Construct_State()\" to define \
                \n    State'
            self.State = msg;       self.tch = msg
            #self.Construct_State()
        self.State_s = 'Expected State'
        # State ############################################################
        self.target_names = ['Normal State', 
            'CV',   'OV',   # Close Choke Valve, Open Choke Valve
            'DIP',  'IIP',  # Decreasing Input Pressures, Increassing
            'VI',           # Viscosity Increase 
            'GI',   'GD',   # Gass Increasse, Gas Decreasse
            'RI',   'RI']   # RPM Increasse, RPM Decreasse
        if self.Mgas.max()*3600 < 0.5:
              self.target_names = self.target_names[:6]
    ########################################################################
    def Adimensionals(self, d):
        # Aquire Variavels #################################################
        P1 = self.P1;   P2 = self.P2;   DP = self.DP    # [Pa]
        Q = self.Q                                      # [m^3/s]
        T = self.T                                      # [N-m]
        T_in = self.Tin2;                               # [C]
        rho = self.rho                                  # [kg/m^3]
        mu = self.mul/1000                              # [kg/m-s]
        omega = self.RPM * (2 * np.pi) / 60             # [rad/s]
        # Calculate Adimensional Variable ##################################
        self.CQ = (Q) * (1/(omega * (d**3)))# Flow Coeficient
        self.CH = (DP/rho) * \
            (1/((omega**2)*(d**2)))         # Head Coeficient
        self.CH1 = (P1/rho) * \
            (1/((omega**2)*(d**2)))         # Head Inlet Coeficient
        self.CH2 = (P2/rho) * \
            (1/((omega**2)*(d**2)))         # Head Outlet Coeficient
        self.CP = (T/rho) * (1/((omega**2)*(d**5))) # Power Coeficient
        self.Eta = ((Q*DP)/T) * (1/omega)   # Efficient
        self.Xw = (mu/rho) * \
            (1/((d**2)*omega))              # InversedRotacionalReynolds
        ####################################################################
        self.CQ_s = '$\Phi$';               self.CH_s = '$\Psi$'
        self.CH1_s = '$\Psi _{in}$';        self.CH2_s = '$\Psi _{out}$'
        self.CP_s = '$\Pi$';                self.Eta_s = '$\eta$'
        self.Xw_s = '$X_{W}$'
    ########################################################################
    def Construct_State(self):
        ####################################################################
        # Control ##########################################################
        ####################################################################
        W = 33                                      # Window
        t = self.t                                  # Tiempo
        Choke = self.Choke; Booster = self.Booster  # Control Ch  - B
        RPM = self.RPM;     Tset = self.Tset        # Control RPM - T
        Pgas = self.Pgas;   Vgas = self.Vgas;       # Control Gas
        Mgas = mean_movil(self.Mgas,W)              # Mass Flux Gas
        MonBif = 1 if Mgas.max()*3600 > 0.5 else -1
        print("    Test Biphasic") if MonBif == 1 else \
            print("    Test Monophasic")
        ####################################################################
        # Determinate kind of fault in test ################################
        ####################################################################
        k = 0;  X = 'Static';   msg = '    0 Static Test Not Fault'
        if MonBif == 1: ############# Biphasic #############################
            for i in range(1,len(t)):
                if Pgas[i] > Pgas [i-1] or Vgas[i] > Vgas[i-1]:
                    msg = 'Mgas Increase' 
                    k = 7;      X = 'Gas';      break   # 4 Mgas Increase
                elif Pgas[i] < Pgas[i-1] or Vgas[i] < Vgas[i-1]:
                    msg = 'Mgas Decrease' 
                    k = 6;      X = 'Gas';      break   # 5 Mgas Decrease
                elif Choke[i] < Choke[i-1]:
                    msg = 'Close Valve' 
                    k = 1;      X = 'Choke';    break   # 1 Close Valve
                elif Choke[i] > Choke[i-1]:
                    msg = 'Open Valve'
                    k = 2;      X = 'Choke';    break   # 8 Open Valve
                elif Booster[i] < Booster[i-1]:
                    msg = 'P Inlet Decrease' 
                    k = 3;      X = 'Booster';  break   # 2 P Inlet Decrease
                elif Booster[i] > Booster[i-1]:
                    msg = 'P Inlet Increase'
                    k = 4;      X = 'Booster';  break   # 9 P Inlet Increase
                elif RPM[i] < RPM[i-1]:
                    msg = 'RPM Decrease' 
                    k = 9;      X = 'RPM';      break   # 6 RPM Decrease
                elif RPM[i] > RPM[i-1]:
                    msg = 'RPM Increase'
                    k = 8;      X = 'RPM';      break   # 7 RPM Increase
                elif Tset[i] < Tset[i-1]:
                    msg = 'Viscosity Increase' 
                    k = 5;      X = 'Temp';     break   # 3 Viscos. Decrease
        else:           ############# Monophasic ###########################
            for i in range(1,len(t)):
                if Choke[i] < Choke[i-1]:
                    msg = 'Close Valve' 
                    k = 1;      X = 'Choke';    break   # 1 Close Valve
                elif Choke[i] > Choke[i-1]:
                    msg = 'Open Valve'
                    k = 2;      X = 'Choke';    break   # 2 Open Valve
                elif Booster[i] < Booster[i-1]:
                    msg = 'P Inlet Decrease'
                    k = 3;      X = 'Booster';  break   # 3 P Inlet Decrease
                elif Booster[i] > Booster[i-1]:
                    msg = 'P Inlet Increase' 
                    k = 4;      X = 'Booster';  break   # 4 P Inlet Increase
                elif Tset[i] < Tset[i-1]:
                    msg = 'Viscosity Increase'
                    k = 5;      X = 'Temp';     break   # 5 Viscos. Decrease
        msg = '    ' + str(k) + ' ' + msg
        print("    Time for Control is t = %s"%t[i])
        ####################################################################
        # Assing limits in what fault happens ##############################
        ####################################################################
        if X == 'Static':       ################# Static ###################
            i1 = 0;                     i2 = 3
        elif X == 'Temp':       ############### Temperature ################
            Tin = mean_movil(                       # Temperatura [C]
                (self.Tin1 + self.Tin2) * 0.5,W)
            Tinvar = [Tin[j:j+W].var() for j in range(0,len(t))]
            E = np.array(2E-5)                      # Max var before control
            i1 = np.where(Tinvar[i:]>E)[0][0] + i   # first higuer than E
            i2 = np.where(Tinvar[i:]>E)[0][-1] + i  # last lower than E
        else:                   ####### Gas, Choke, Booster and RPM ########
            # Initial Functions
            def get_variables(X,W):
                X = mean_movil(X,W)
                Xvar = [X[j:j+W].var() for j in range(0,len(X))]
                Xvarg = np.gradient([X[j:].var() for j in range(0,len(X))])
                return X, Xvar, Xvarg
            First = lambda x : x[0] if len(x) > 0 else 0
            # Intitial Variables
            P1, P1var, P1varg = get_variables(self.P1,W)
            P2, P2var, P2varg = get_variables(self.P2,W)
            M, Mvar, Mvarg = get_variables(self.M,W)
            if X == 'Gas':                                           # Gas #
                # Get Gas Mass Variance and his first derivate
                Mgas, Mgasvar, Mgasvarg = get_variables(self.Mgas, W)
                # Functions only used in Gas
                Last = lambda x : x[-1] if len(x) > 0 else 0
                def gas_limits(Xvar,i):
                    I = np.argmax(Xvar[i:])             # Index Max Variance
                    Xd = np.gradient(Xvar[i:])          # 1st Diff Variance
                    i1 = Last(np.where(Xd[0:I-1]<0)[0]) + i
                    i2 = First(np.where(Xd[I+1:]>0)[0]) + I + i
                    return i1, i2                       # Limits pos
                # Get posible for every posible variable
                i1P1, i2P1 = gas_limits(P1var, i)
                i1P2, i2P2 = gas_limits(P2var, i)
                i1M,  i2M  = gas_limits(Mvar, i)
                i1Mgas, i2Mgas = gas_limits(Mgasvar, i)
                # Firs limit when variance of begin to increasse 
                i1 = min(i1P1, i1P2, i1M, i1Mgas)
                # Last limit when variance of finish to decreasse
                i2 = max(i2P1, i2P2, i2M, i2Mgas)
            else:                                 # Choke, Booster and RPM #
                # One seconde more to choke valve
                i = i + 33 if X == 'Choke' else i  
                # Commun Functions for Choke Booster and RPM
                WhereMax = lambda X, i : First(
                    np.where(X[i:] > np.max(X[0:i]))[0] ) + i
                Change = lambda X, i : np.max([ First(np.where(X[i:]<0)[0]), 
                    First(np.where(X[i:]>0)[0]) ])
                I2 = lambda X, i, t : X + i if X !=0 else len(t) - 1
                # Get first limit when Variance begin to increasse
                i1P1 = WhereMax(P1var,i)
                i1P2 = WhereMax(P2var,i)
                i1M = WhereMax(Mvar,i)
                i1 = min(i1P1, i1P2, i1M)
                #Get las limit whenn Variance is so slow for every variable
                i2P1 = I2(Change(P1varg,i1), i1, t)
                i2P2 = I2(Change(P2varg,i1), i1, t)
                i2M =  I2(Change(Mvarg,i1), i1, t)
                i2 = max(i2P1, i2P2,i2M)
        tch = [t[i1],t[i2]]                             # Time to change
        S = np.zeros(len(t))                            # State
        for i in range(i1,i2+1): S[i] = k               # put State
        # Check if exist any posible error
        if int(S.max()) != int(k):   msg = 'Error in Fault Simulation Test'
        print("%s\n\n"%msg)
        # Save Variables
        self.State = S; self.tch = tch; self.ich = [i1,i2]
    ########################################################################
    def Plots(self, Kind, Pred, MonBif):
        t = self.t; t_s = self.t_s
        # secondary Axis for States ########################################
        s = [self.State];   s_s = [self.State_s];   s_c = [(0.2,0,0)]
        if len(Pred)>0:
            s.append(Pred);                 s_s.append("Predicted State")
            s_c.append((0.3,0,0))
        yt = 9 if self.Mgas.max()*3600 > 0.5 else 5
        #yt = 9 if MonBif == "Bif" else 5;   
        Yt = [i for i in range(0,yt+1)]
        Yl = [-0.1, np.max(Yt) + 0.1]   # Define Secondary ticks and Limits
        #Yt = 0; Yl = 0
        # Primary Axis #####################################################
        if Kind == 'temperature':
            x = [self.Tin1, self.Tin2, self.T2, self.T4, self.T6, self.T8, 
                self.Tout1, self.Tout2, self.Tset]
            x_s = [self.Tin1_s, self.Tin2_s, self.T2_s, self.T4_s, 
                self.T6_s, self.T8_s, self.Tout1_s, self.Tout2_s, 
                self.Tset_s]
            x_c = [(0.3,0.3,0.3), (0.3,0.3,0.3), (0.4,0.4,0.4), 
                (0.4,0.4,0.4), (0.5,0.5,0.5), (0.5,0.5,0.5), 
                (0.6,0.6,0.6), (0.6,0.6,0.6), (0,0,0)]
            x_t = ['--x', '--+', ':o', ':d', ':s', ':v', '--x', '--+', ':']
            Axes = "Equal"
        elif Kind == 'delta_pressure':
            x = [self.DP1, self.DP2, self.DP3, self.DP4, self.DP5, 
                self.DP6, self.DP7, self.DP8, self.DP9, self.DP10]
            x_s = [self.DP1_s, self.DP2_s, self.DP3_s, self.DP4_s, 
                self.DP5_s, self.DP6_s, self.DP7_s, self.DP8_s, 
                self.DP9_s, self.DP10_s]
            x_c = [(0.3,0.3,0.3), (0.45,0.45,0.45), (0.3,0.3,0.3), 
                (0.525,0.525,0.525), (0.375,0.375,0.375), 
                (0.525,0.525,0.525), (0.375,0.375,0.375), (0.6,0.6,0.6), 
                (0.45,0.45,0.45), (0.6,0.6,0.6)]
            x_t = [':x', '-x', ':o', '-o', ':s', '-v', ':+', '-+',":d",'-d']
            Axes = "Equal"
        elif Kind == 'pressure':
            x = [self.P1*1E-5, self.P2*1E-5, self.DP*1E-5]
            x_s = ['P1 [Bar]', 'P2 [Bar]', '$\Delta P\ [Bar]$']
            x_c = [(0.4,0.4,0.4), (0.3,0.3, 0.3), (0.6,0.6,0.6)]
            x_t = [':x', '--+', '-o']
            Axes = "Equal"
        elif Kind == 'gas':
            x = [self.Mgas*3600, self.Pgas, self.Vgas]
            x_s = ['$\dot M_{gas}\ [kg/h]$', self.Pgas_s, self.Vgas_s]
            x_c = [(0.6,0.6,0.6), (0.4,0.4,0.4), (0.3,0.3,0.3)]
            x_t = ['-o', ':x', ':+']
            Axes = "Diff"
        elif Kind == 'flow':                    # Flow
            x = [self.M*3600, self.T]
            x_s = ['$\dot M\ [kg/h]$', self.T_s]
            x_c = [(0.3,0.3,0.3), (0.5,0.5,0.5) ]
            x_t = [':o', '-x']
            Axes = "Diff"
        else:                                   # Control
            x = [self.Choke, self.Booster, self.RPM]
            x_s = [self.Choke_s, self.Booster_s, self.RPM_s]
            x_c = [(0.4,0.4,0.4), (0.3,0.3, 0.3), (0.6,0.6,0.6)]
            x_t = [':x', '--+', '-o']
            Axes = "Diff"
        Fig = Plot(t,t_s,x,x_s,x_c,x_t,s,s_s,s_c,Axes,Yt,Yl)
        return Fig
    ########################################################################
    def get_X(self,Var,W,Gr):
        ''' Var is list in form ['P1','P2', ...] \nW is a int 
        \nGr is a string \'X\' = only Var \'Xd\' = only first derivat of Var
        \'\' Both derivate Var and derivate of Var '''
        X = np.empty([len(self.t),0]);  X_s = []    # Variable
        Xd = np.empty([len(self.t),0]);  Xd_s = []  # Derivate
        Xi = np.empty([len(self.t),0]);  Xi_s = []  # Integer
        for i in Var:                               # for in Variables
            x = mean_movil(self.__getattribute__(i),W)  # Get Variable mean
            X = np.c_[X, x]                         # Add Variables to frame
            x_s = self.__getattribute__(i + '_s')   # Get Variable Name
            X_s.append(x_s)                         # Add Name to list names
            if 'Xd' in Gr:                          # If Add Gradients
                xd = gradient_movil(x,self.t,W)     # gradient of Variables
                Xd = np.c_[Xd, xd]                  # Add gradients
                xd_s = x_s.replace(']','/s]')       # Transform Strig from
                xd_s = xd_s.replace('\\',' /dt\\')  #   Variable to get
                xd_s = '$d ' + xd_s[1:]             #   gradient string
                xd_s = xd_s.replace('d  /dt','d ')  # Correct erros
                xd_s = xd_s.replace('/s/s','/s^2')  # Change time string
                Xd_s.append(xd_s)                   # Add gradient strings
            if 'Xi' in Gr:                          # If Add Integers
                xi = integer_movil(x,self.t,W)      # integer of variables
                Xi = np.c_[Xi,xi]                   # Add integers
                xi_s = x_s.replace(']','-s]')       # Transform Strig from
                xi_s = xi_s.replace('\\',' dt\\')   #   Variable to get
                xi_s = '$\int ' + xi_s[1:]          #   integer string
                xi_s = xi_s.replace('  dt',' ')     # Correct erros
                xi_s = xi_s.replace('/s-s','')      # change time String
                Xi_s.append(xi_s)                   # Add integer strings
        X = X if 'X' in Gr else np.empty([len(self.t),0])
        self.X = np.c_[X, Xd, Xi]
        X_s = X_s if 'X' in Gr else []
        self.X_s = X_s + Xd_s + Xi_s                # Get Self X derivates
        return self.X, self.State

############################################################################
#   Get Train, Validation and Test Data ####################################
############################################################################
def Construct(Index,folder,List,W,Var,Gr,F):
    X_len = len(Var)*len(Gr);       k = 1                   # Len Data
    c = 2 if ('Xd' in Gr or 'Xi' in Gr) else 1
    data_X = np.empty([0,X_len]);   data_y = np.empty([0,]) # Initial data
    print('\n    Contruct from %s data'%(len(Index)))
    for i in Index:                                 # Search data by index
        sys.stdout.write("\r        Construct X:... {:.2f} %"\
            .format(100*(k)/len(Index)))
        Sample_a = np.load(folder + List[int(i)][0] + '.npy') # Sample Array
        Sample_o = Sample_lvm(Sample_a)             # Sample as Object
        Sample_o.get_X(Var,W,Gr)                    # Get X data in Object
        s = Sample_o.State.max()                    # State in Sample
        X = Sample_o.X[W*c:];       y = Sample_o.State[W*c:]    # Cut X
        i1 = np.where(y==s)[0][0];  i2 = np.where(y==s)[0][-1]  # begin, end
        X = X[i1:i2+1];             y = y[i1:i2+1]              # X, y
        data_X = np.r_[data_X,X];   data_y = np.r_[data_y,y]    # Add X, y
        k+=1                                        # Nex iteration
    X_s = Sample_o.X_s;                             # Get Feature Names
    y_s = list(np.array(Sample_o.target_names)[F])  # Get Target Names
    data = {'data': data_X,               'target': data_y,     # Save Dict
            'feature_names': X_s,   'target_names': y_s}        # Data
    return data                                     # Return Results
############################################################################
def plot_confusion_matrix(cm, classes, normalize=False,
    title='',          cmap=plt.cm.Greys, 
    fontsize=20):
    """ This function prints and plots the confusion matrix. Normalization 
    can be applied by setting `normalize=True`."""
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        #print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    #print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title,fontsize=fontsize)
    #plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black",
                 fontsize=fontsize)

    plt.tight_layout()
    plt.ylabel('True label',fontsize=fontsize)
    plt.xlabel('Predicted label',fontsize=fontsize)
############################################################################
def MetricS(tree,X,y):
    y_pred = tree.predict(X)                    # Prediction Values
    cm = confusion_matrix(y,y_pred)             # Confusion Matrix
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    cm_mean = cm.diagonal().mean() # Mean Failure detected
    cm_min = cm.diagonal().min()    # Worst Failure detected
    sts = tree.score(X, y)           # Accuracy
    return sts, cm_mean, cm_min, cm
############################################################################
def TrainValTest(Data,i_v,i_t):
    X_va = Data[i_v]['data'];  y_va = Data[i_v]['target']
    X_te = Data[i_t]['data']; y_te = Data[i_t]['target']
    l = Data[i_t]['data'].shape[1]
    I = list(Data.keys());  I.remove(i_t);  I.remove(i_v)
    X_tr = np.empty([0,l]);   y_tr = np.empty([0,])
    for i in I:
        X_tr = np.r_[X_tr,Data[i]['data']]
        y_tr = np.r_[y_tr,Data[i]['target']]
    X = {'Train': X_tr, 'Val':X_va, 'Test':X_te}
    y = {'Train': y_tr, 'Val':y_va, 'Test':y_te}
    return X, y

############################################################################
# Functions to Filter ######################################################
############################################################################
def mean_movil(Vector,Window):                  # Mean Movil Filter ########
    Vector_new = []
    for i in range(len(Vector)):
        if i<Window:
            m = np.mean(Vector[:Window])
        else:
            m = np.mean(Vector[i-Window:i])
        Vector_new.append(m)
    Vector_new = np.array(Vector_new)
    return Vector_new
############################################################################
def gradient_movil(Vector,t,Window):            # Mean Movil Gradient ######
    grad = []
    for i in range(len(Vector)):
        if i<Window:
            m = np.mean(np.gradient(Vector[:Window],t[:Window]))
        else:
            m = np.mean(np.gradient(Vector[i-Window:i],t[i-Window:i]))
        grad.append(m)
    grad = np.array(grad)
    return grad
############################################################################
def integer_movil(Vector,t,Window):             # Mean Movil Gradient ######
    integer = lambda V,t: np.array([0.5*(V[i]+V[i-1])*(t[i]-t[i-1]) 
        if i!= 0 else 0 for i in range(len(V))])
    inte = []
    for i in range(len(Vector)):
        if i<Window:
            m = integer(Vector[:Window],t[:Window]).sum() \
                - Vector[i]*(t[i]-t[i-Window])
        else:
            m = integer(Vector[i-Window:i],t[i-Window:i]).sum() \
                - Vector[i]*(t[i]-t[i-Window])
        inte.append(m)
    inte = np.array(inte)
    return inte
############################################################################
def index_data(Samples,MonBif,m,Fault,f):       # filter data in list ######
    '''Samples = List where have to be search the coincidences.
    MonBif = \"Mon\" or \"Bif\" m = 1
    MonBif = \"Mo2\" or \"Bi2\" m = 3
    Fault = \'GasCha\', \'ChoClo\', \'ChoOpe\', \'GasCha\', \'PreDec\', 
            \'PreInc', 'RpmCha', \'Static\', \'Unclasf\' or \'VisInc\' 
            f = 2
    Fault = \'0.0\', \'1.0\', \'2.0\', \'3.0\', \'4.0\', \'5.0\', \'6.0\',
             \'7.0\', \'8.0\' or \'9.0\' 
            f = 4'''
    if MonBif == "":    # Samples Index where MonBif is anyone
        if Fault == "": # Samp. Ind. Where MonBif and Fault is anyone
            Index = np.arange(0,Samples.shape[0])
        else:           # Samp. Ind. where have Fault and MonBif is anyone
            Index = np.where(Samples[:,f] == Fault)[0]
    else:               # Samples Index where Fault is anyone 
        Index_MB = np.where(Samples[:,m] == MonBif)[0]
        if Fault == "": # Samp. Ind. where have MonBif and Fault is anyone 
            Index = Index_MB
        else:           # Samp. Ind. where have MonBif and Fault both
            Samples_MB = Samples[Index_MB]
            Index_F = np.where(Samples_MB[:,f] == Fault)[0]
            Index = Index_MB[Index_F]
    return Index

############################################################################
# Plots ####################################################################
############################################################################
def Plot(t,t_s,x,x_s,x_c,x_t,sec,sec_s,sec_c,Axes,Yticks,Ylim,
    fontsize='xx-large', labelsize='large',labelrotation=0., markersize=10,
    markerevery=0, linewidth=1):
    '''t     = numpy vector of time,     t_s   = str to t (horizontal axis)
    x     = list of numpy arrays in left vertical axis, 
    x_s   = list str or x
    x_c   = list of color for x,      x_t   = list of styles of x.
    Sec   = list of numpy arrays in right vertical axis, 
    sec_s = list strings or x,        sec_c = list of styles of sec.
    Axes  = could be "Equal" to the same scale of x variables or "Diff"  
              to different scale to x variables.
    Yticks= ticks to right axis if Yticks == 0 ticks default.
    Ylim  = limits to right axis if Ylim == 0 ticks default.'''
    fig, ax = plt.subplots(1,1, figsize=(15,7.5))
    me = (int(len(t)/20) if len(t)>=20 else 1) \
        if markerevery==0 else markerevery
    ms = markersize;    lw=linewidth
    # Mean #################################################################
    ax.minorticks_on()                  # Major and Minor Ticks
    ax.grid(which='major', linestyle='-', linewidth=0.5, color=(0.8,0.8,0.8))
    ax.grid(which='minor', linestyle=':', linewidth=0.5, color=(0.9,0.9,0.9))
    ax.set_xlabel(t_s, fontsize=fontsize)
    if Axes == "Equal":                 # Same Scales
        for i in range(0,len(x)):           # Plot Every Variable 
            ax.plot(t, x[i], x_t[i], color=x_c[i], label=x_s[i], \
                markersize=ms, markevery=me, linewidth=lw, markeredgewidth=lw
                )# Mean Axes Plot
        ax.legend(loc=(0.0,1.0), fontsize=fontsize, ncol=5)
        xmax = np.max(x);   xmin = np.min(x)
        dx = xmax - xmin                    # Limits Y Axis
        ax.set_ylim( (xmin - 0.05*dx, xmax + 0.05*dx) )
        ax.tick_params(axis='both',labelsize=labelsize)
        ax.tick_params(axis='y',labelrotation=labelrotation)
    else:                               # Diferent Scales
        lns = ax.plot(t,x[0], x_t[0], color=x_c[0], label=x_s[0], 
            markevery=me, markersize=ms, linewidth=lw,
            markeredgewidth=lw)
        ax.set_ylabel(x_s[0], fontsize=fontsize)#, color=x_c[0])
        #ax.tick_params('y', colors=x_c[0])  # Mean Axis Plot firs Variable
        xmax = np.max(x[0]);   xmin = np.min(x[0]);   
        dx = xmax - xmin                    # Limits Y Axis
        ax.set_ylim( (xmin - 0.05*dx, xmax + 0.05*dx) )
        ax.tick_params(axis='both',labelsize=labelsize)
        ax.tick_params(axis='y',labelrotation=labelrotation)
        #ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.1e'))
        #ax.yaxis.set_major_locator(ticker.AutoLocator())
        #ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
        #ax.yaxis.set_major_formatter(
        # ticker.ScalarFormatter(useMathText=True))
        ax1 = [plt.plot(0,0)]*(len(x)-1);   lnm = [plt.plot(0,0)]*(len(x)-1)
        k = 0                               # Initial Value Legend & Axis
        for i in range(0,len(ax1)):         # Plot Every Other Variable
            k -= 0.09                           # Space Between Axis
            ax1[i], lnm[i] = plot_secondary(ax,t,x[i+1],x_s[i+1],x_c[i+1], \
                x_t[i+1] ,0,0,"left",me,ms,k,
                    fontsize,lw)                # Get Axis and Legend
            lns = lns + lnm[i]                  # Concatenate Legend
            xmax = np.max(x[i+1]);   xmin = np.min(x[i+1]);   
            dx = xmax - xmin                    # Limits Y Axis
            ax1[i].set_ylim( (xmin - 0.05*dx, xmax + 0.05*dx) )
            ax1[i].tick_params(axis='both',labelsize=labelsize)
            ax1[i].tick_params(axis='y',labelrotation=labelrotation)
            #ax1[i].yaxis.set_major_formatter(
            # mtick.FormatStrFormatter('%.1e'))
            #ax1[i].xaxis.set_major_locator(ticker.AutoLocator())
            #ax1[i].xaxis.set_minor_locator(ticker.AutoMinorLocator())
            #ax1[i].xaxis.set_major_formatter(
            # ticker.ScalarFormatter(useMathText=True))
        labs = [l.get_label() for l in lns] # Get and Plot Legend
        ax.legend(lns, labs, loc=(0.0,1.0), fontsize=fontsize,ncol=i+2)
        #plt.subplots_adjust(left=-k)
    
    # Secondary ############################################################
    if sec != 0:                        # If There are Secondary axys
        string = '$-\!\!\!-\!\!\!\!-$ ' + sec_s[0]
        axs = plot_secondary(ax,t,sec[0],string, sec_c[0], '-', Ylim, 
            Yticks, "right", me, ms, 1,fontsize,lw
            )                               # Plot Secondary axis 1
        if len(sec) == 1:                   # Set label y
            axs.set_ylabel(sec_s[0], color=sec_c[0], fontsize=fontsize)
            axs.tick_params(axis='both',labelsize=labelsize)
            axs.tick_params(axis='y',labelrotation=0.)
        elif len(sec) == 2:                 # Plot Secondary axis 2
            axs.plot(t, sec[1], '--', color=sec_c[1], label=sec_s[1], linewidth=lw)
            axs = axs_text(axs,sec_s[0],sec_c[0],'top',fontsize)
            axs = axs_text(axs,sec_s[1],sec_c[1],'bottom',fontsize)
            axs.tick_params(axis='both',labelsize=labelsize)
            axs.tick_params(axis='y',labelrotation=0.)
        else:                               # More than 2 axis
            print("Warnnig: maximum of two lines on secondary axis")
    plt.tight_layout(pad=1.08)
    plt.subplots_adjust(top=0.91)
    return fig
############################################################################
def axs_text(axs,sec_s,sec_c,topbottom,fontsize
    ):                                      # Text to secondary axis #######
    strtex = '$-\!\!\!-\!\!\!\!-$ ' + sec_s if topbottom == 'top' \
        else '   ----- '      + sec_s
    axs.text(0.995, 0.5, strtex ,horizontalalignment='right', 
        transform=axs.transAxes, verticalalignment=topbottom, 
        rotation='vertical', color=sec_c,fontsize=fontsize)
    return axs
############################################################################
def plot_secondary(ax,t,x,x_s,x_c,x_t,Ylim,Yticks,Loc,me,ms,k,fontsize,lw
    ):                                                          # Sec ######
    axs = ax.twinx();                                           # 
    if Ylim != 0: axs.set_ylim(Ylim[0], Ylim[1])
    if Yticks != 0: axs.set_yticks(Yticks)
    #axs.tick_params('y', colors=x_c)
    lns = axs.plot(t, x, x_t, color=x_c, label=x_s, markevery=me, \
        markersize=ms, linewidth=lw, markeredgewidth=lw)
    axs.spines[Loc].set_position(("axes", k))
    if Loc == "right":  
        return axs
    else:   # "left"
        axs.set_ylabel(x_s, fontsize=fontsize) #color=x_c,
        axs.yaxis.set_label_position(Loc)
        axs.yaxis.tick_left()
        return axs, lns

############################################################################
# Propierties of Liquid ####################################################
############################################################################
def Mul(T):
    a = 14.06646543;    b = -586161.2888;   c = 491531.6781
    d = -170278.8512;   e = 31562.04855;    f = -3391.691042
    g = 214.5231787;    h = -102.6877725
    mu = a*(math.e**((b + (c*T) + d*(T**2) + e*(T**3) + f*(T**4) \
        + g*(T**5))/(T**6)))+ h
    return mu   #[Cp]

def Mug(T):
    a = 14.06646543;    b = -586161.2888;   c = 491531.6781
    d = -170278.8512;   e = 31562.04855;    f = -3391.691042
    g = 214.5231787;    h = -102.6877725
    mu = a*(math.e**((b + (c*T) + d*(T**2) + e*(T**3) + f*(T**4) \
        + g*(T**5))/(T**6)))+ h
    return mu   #[Cp]

def Rhol(T):
    rho = -0.637*T + 961.95
    return rho # [kg/m^3]

def Rhog(T,P):
    R_n2 = 2968                         # R (N_2) [J/Kg-C]
    rho = P / (R_n2*(T + 273.15)) 
    return rho # [kg/m^3]
