#%%#########################################################################
#   Import libraries #######################################################
############################################################################
import numpy as np 
import Functions as fn
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
from sklearn.metrics import confusion_matrix
import graphviz
savefolder = '2_Figures/'

#%%#########################################################################
#    Figure 03 Data
############################################################################
folder = '1_Results/dt/'
List = np.load(folder + '02_SelectedSamples.npy')   # List of Data
Index = np.load(folder + '02_FaultIndex.npy').item()
folderload = '0_Data/2_Samples/'
savefolder = '2_Figures/'
cr = 'entropy'
cw = 'balanced'
G_Data = np.load(folder + '03_GroupData.npy').item()
G_Index = np.load(folder + '03_GroupIndex.npy').item()

#G_Data[0]['feature_names'][5+9]
# Get Data ==================================================================
RPM1 = np.array([1860])
RPM2 = np.array([2440,2441,2443,2450,2480])
RPM3 = np.array([3050, 3060])
RPM4 = np.array([3500, 3540, 3547, 3549, 3550, 3560])
F = [0,1,3,5,7];    Frame = {}
for f in F:                                 # For in Faults
    Frame[f] = {'X':[], 'y':[], 's':[], 'Table':[]}     # Frame, Matrix, Target, Fault
    for i in Index['Bi2'][f]:
        Sample_f = np.load(folderload + List[i,0] + '.npy') # Load Sample
        Sample_o = fn.Sample_lvm(Sample_f)                  # As object
        S = Sample_o.State.max()                            # Kind Fault
        State = (((S==0)*1)*'Regular State' + ((S==1)*1)*'CV'   # State
                + ((S==3)*1)*'DIP' + ((S==5)*1)*'IV' + ((S==7)*1)*'IG')
        RPM = Sample_o.RPM.mean()                           # RPM
        RPM = (any(RPM1==RPM)*1800 + any(RPM2==RPM)*2400 + 
             any(RPM3==RPM)*3000 + any(RPM4==RPM)*3500)     # Filer RPM
        DP = Sample_o.DP.mean()*1E-5                        # Delta P
        M = Sample_o.M.mean()                          # Mass Flow
        Mgas = Sample_o.Mgas.mean()*3600

        DP1 = Sample_o.DP.min()*1E-5                        # Delta P
        M1 = Sample_o.M.min()*3600                          # Mass Flow
        DP2 = Sample_o.DP.max()*1E-5                        # Delta P
        M2 = Sample_o.M.max()*3600                          # Mass Flow

        T = Sample_o.Tin.mean()                             # Inlet Temp
        if S == 1:                                          # Get Changes
            C1 = Sample_o.Choke.min();      C2 = Sample_o.Choke.max()
            Y = 'CV'
        elif S == 3:
            C1 = Sample_o.Booster.min();    C2 = Sample_o.Booster.max()
            Y = 'DIP'
        elif S == 5:
            C1 = Sample_o.Tset.min();       C2 = Sample_o.Tset.max()
            Y = 'IV'
        elif S == 7:
            C1 = Sample_o.Mgas.min()*3600;  C2 = Sample_o.Mgas.max()*3600
            Y = 'IG'
        else:
            C1 = 0;                         C2 = 0
            Y = 'Regular State'
        CD = C2 - C1
        Frame[f]['X'].append([RPM, DP, M, T,C1,C2, CD ])
        Frame[f]['Table'].append([
            Sample_o.Tset.mean().round(), 
            RPM, 
            Mgas.round(), 
            M.round(), 
            DP1, DP2, M1, M2])
        Frame[f]['y'].append(Y)
        Frame[f]['s'].append(S)
    else:
        Frame[f]['X'] = np.array(Frame[f]['X'])
        Frame[f]['y'] = np.array(Frame[f]['y'])
        Frame[f]['s'] = np.array(Frame[f]['s'])
        Frame[f]['Table'] = np.array(Frame[f]['Table'])
#%% Plot  Graphs ============================================================
fig = plt.figure(figsize=(22,11))
ax = plt.subplot2grid((2, 2), (0, 0), rowspan=2)
ax1 = plt.subplot2grid((2, 2), (0, 1))
ax2 = plt.subplot2grid((2, 2), (1, 1))
x_s = ['Normal', 'CV', 'DIP', 'VI', 'GI']
x_t = ['.', 'd', 's', 'o', '+']
x_c = [(0,0,0), (0,0.2,0.1), (0,0.2,0.4), (0.4,0,0), (0.4,0.2,0)]
for i in [0,4,3,2,1]:
    f = F[i]
    ax.plot(Frame[f]['X'][:,2],Frame[f]['X'][:,1],x_t[i], color=x_c[i], 
        label=x_s[i], alpha=0.5, markersize=15)
ax.legend(loc=2, fontsize=20, ncol=1)
ax.set_xlabel('$\dot{m}$ [kg/s]', fontsize=30)
ax.set_ylabel('$\Delta p \ [Bar]$', fontsize=30)
ax.tick_params(axis='both',labelsize=20)
ax.set_ylim( (0, 18) )
ax.set_xlim( (0, 40) )
# Plot 02 ##################################################################
bins = np.linspace(29,51,23)
for i in [0,4,3,2,1]:
    f = F[i]
    ax1.hist(Frame[f]['X'][:,3], bins=bins, fill=True, stacked=True, 
        histtype='step', color=x_c[i],label=x_s[i], alpha=0.5)
ax1.legend(loc=2, fontsize=20, ncol=1)
ax1.set_xlabel('$\mathrm{T_{in}}$ [$^\circ$c]', fontsize=30)
ax1.set_ylabel('# Experiments', fontsize=30)
ax1.tick_params(axis='both',labelsize=20)
ax1.set_ylim( (0, 100) )
# Plot 03 ##################################################################
RPM_Count = {}
for f in F:
    RPM_Count[f]= []
    for i in [1800,2400,3000,3500]:
        j = np.where(Frame[f]['X'][:,0]==i)[0].shape[0]
        RPM_Count[f].append(j)
t_s = np.array([1800,2400,3000,3500])
w = 50; k = 0
for i in [0,4,3,2,1]:
    f = F[i]
    ax2.bar(t_s+((-2*w)+(w*k)), RPM_Count[f], w, color=x_c[i], 
        label=x_s[i], alpha=0.5);  k+=1
ax2.set_xticks(t_s)
#ax2.legend(loc=2, fontsize='xx-large', ncol=1)
ax2.set_xlabel('$\omega$ [rpm]', fontsize=30)
ax2.set_ylabel('# Experiments', fontsize=30)
ax2.tick_params(axis='both',labelsize=20)
ax2.set_ylim( (0, 140) )
plt.tight_layout()
fig.set_rasterized(True);   ax.set_rasterized(True)
ax1.set_rasterized(True);   ax2.set_rasterized(True)
plt.savefig(savefolder + 'data.pdf',rasterized=True,dpi=300)


#%%#########################################################################
# Figure 8 and 9  Global Plots
############################################################################
savefolder = '2_Figures/'
def GraphGlobal(MLN_Trend,Test,stop):
    criteria = stop
    MLN = np.array(MLN_Trend["MLN"])
    X_Plot = {}; count = 0
    # Validation and  Train Results [mln, group, nest] = [0, 1, 2] ---------
    Train_r = np.array(MLN_Trend["Train"][stop])
    Val_r = np.array(MLN_Trend["Val"][stop])
    # For every MLN get respective best n_estimators -----------------------
    Nest_i = [np.bincount(i).argmax() for i in Val_r.argmax(2)] # Index Est.
    # Get Validation -------------------------------------------------------
    Val = Val_r.max(2).mean(1)
    Val_std = Val_r.max(2).std(1)
    # Get Train ------------------------------------------------------------
    Tra = Train_r.max(2).mean(1)
    # Save Data ------------------------------------------------------------
    X_Plot[stop] ={'Train':Tra, 'Val':Val, 'Val_std':Val_std,
        'Nest_i':Nest_i}
    x_s = ['Train', '', 'Validation', '', 'Test']
    x_c = [(0,0,0), (0.5,0,0), (0.5,0,0), (0.5,0,0), (0.4,0.4,0.4)]
    x_t = [":", "_", '-+', "_", "--o"]
    Tra = X_Plot[criteria]['Train']
    Val = X_Plot[criteria]['Val']
    Val_std = X_Plot[criteria]['Val_std']
    Nest_i = X_Plot[criteria]['Nest_i']
    Test_c = np.array(
        [Test[criteria][i][Nest_i[i]] for i in range(0,len(Nest_i))])
    x = [Tra, Val-Val_std/2, Val, Val+Val_std/2, Test_c]
    plt.clf()
    Fig1 = fn.Plot(MLN,'Max Leaf Nodes $mln$',x,x_s,x_c,x_t,0,0,0,'Equal',0,0,
    fontsize=25, labelsize=20, markersize=10, linewidth=2.5)
    ylab = '$cm_{mean}$' if stop== 'cmmean' else '$sts$'
    plt.ylabel(ylab, fontsize=25)
    plt.tight_layout(pad=1.08)
    plt.subplots_adjust(top=0.91)
    plt.savefig(savefolder + criteria + ".pdf")
# Plot Trend Detection Tree Figure 8. ======================================
folder = '1_Results/dt/entropy/'
MLN_Trend = np.load(folder + 'MLN_Trend.npy').item()
Test = np.load(folder + 'Test.npy').item()
GraphGlobal(MLN_Trend,Test,'sts')
# Plot Trend Classification Tree Figure 9. =================================
folder = '1_Results/cdt/gini/'
MLN_Trend = np.load(folder + 'MLN_Trend.npy').item()
Test = np.load(folder + 'Test.npy').item()
GraphGlobal(MLN_Trend,Test,'cmmean')

#%%#########################################################################
#  Figures Desicion Tree Algorithm Mean test
############################################################################
folder = '1_Results/dt'
folder_cdt = '1_Results/cdt'
List = np.load(folder + '/02_SelectedSamples.npy')   # List of Data
Index = np.load(folder + '/02_FaultIndex.npy').item()
G_Data = np.load(folder + '/03_GroupData.npy').item()
G_Index = np.load(folder + '/03_GroupIndex.npy').item()
cr_d = 'entropy';          cw_d = 'balanced';      criteria_d = 'sts'
cr_c = 'gini';          cw_c = 'balanced';      criteria_c = 'cmmean'
# Get Data =================================================================
X, y = fn.TrainValTest(G_Data,0,5)
X_train = np.r_[X["Val"],X["Train"]]#[:,Vector]
X_test = X['Test']#[:,Vector]
y_train = np.r_[y["Val"],y["Train"]]
feature_names = np.array(G_Data[0]['feature_names'])#[Vector]
target_names = G_Data[0]['target_names']
tree_d = np.load(folder + '/' + cr_d + '/' + 
    criteria_d + '_Selected_Tree.npy').item()
tree_c = np.load(folder_cdt + '/'+ cr_c + '/' + 
    criteria_c + '_Selected_Tree.npy').item()
# Configure Combined Method ================================================
y_expected = y['Test']
y_predict_d = tree_d.predict(X_test)
y_predict_c = tree_c.predict(X_test)
i_fault = np.where(y_predict_d!=0)[0]
y_predict = y_predict_d.copy()
for i in i_fault:
    y_predict[i] = y_predict_c[i]
savefolder = '2_Figures/'
def Plot_save_cm(y_expected, y_predict, tn, savefolder, 
    labelsize=25, fontsize=30, figsize=(11,11)):
    cm = confusion_matrix(y_expected,y_predict)
    fig = plt.figure(figsize=figsize)
    plt.tick_params(axis='both',labelsize=labelsize)
    fn.plot_confusion_matrix(cm,tn,normalize=True, fontsize=fontsize)
    plt.subplots_adjust(top=0.91)
    plt.tight_layout(pad=1.08)
    plt.savefig(savefolder + '.pdf',rasterized=True,dpi=300)
HealthFault = lambda y: np.array(
        [1 if i == 3 or i == 5 or i == 7 else i for i in y])
# Plot Confusin Matrix =====================================================
tn = ['Normal', 'CV', 'DIP', 'VI', 'GI']
cm = confusion_matrix(y_expected,y_predict)
cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
cm_mean = cm.diagonal().mean() # Mean Failure detected
cm_min = cm.diagonal().min()    # Worst Failure detected
Vector  = y_expected == y_predict
sts = np.where(Vector==True)[0].shape[0]/len(Vector)
print(sts)
print(cm_mean)
print(cm_min)
Plot_save_cm(y_expected, y_predict, tn, savefolder + 'matrix_post')
# Plot Confusin Matrix Without Preprocessing ===============================
Plot_save_cm(y_expected, y_predict_d, tn, savefolder + 'matrix')
# Plot Confusin Matrix for only Detection ==================================
y_test2 = HealthFault(y['Test'])
y_predict2 = HealthFault(y_predict_d)
tn = ['Normal', 'Fault']
Plot_save_cm(y_test2, y_predict2, tn, savefolder + 'matrix_detec',
    figsize=(5.5,5.5))
# Plot Confusin Matrix for only Detection Without Preprocessing ============
y_predict2 = HealthFault(y_predict)
tn = ['Normal', 'Fault']
Plot_save_cm(y_test2, y_predict2, tn, savefolder + 'matrix_detec2',
    figsize=(5.5,5.5))
#%% Plot Feature Importance ================================================
fig = plt.figure(figsize=(12,12))
Importance = tree_c.feature_importances_
n = len(Importance)
plt.barh(range(n),Importance*100,align='center',color=(0,0,0))
plt.tick_params(axis='both',labelsize=30)
feature_names = [
    '$p_{in}$', '$p_{out}$', '$\\Delta p$',
    '$\dot{m}$', '$\\tau $', '$T_{in}$',
    '$T_{out}$', '$W$', '$\eta$',
    '$\delta p_{in} /\delta t$', '$\delta p_{out} /\delta t$',
    '$\delta \\Delta p /\delta t$', '$\delta \dot{m} /\delta t$',
    '$\delta \\tau /\delta t$', '$\delta T_{in} /\delta t$',
    '$\delta T_{out} /\delta t$', '$\delta W /\delta t $',
    '$\delta \eta /\delta t$']
plt.yticks(np.arange(n),feature_names)
plt.xlabel("Feature importance %",fontsize=30)
plt.ylabel("Feature",fontsize=30)
plt.subplots_adjust(top=0.91)
plt.tight_layout(pad=1.08)
plt.savefig(savefolder + 'importance.pdf',rasterized=True,dpi=300)
tree_c
#%% Esport Tree ===========================================================
feature_names = ['Pin [Pa]', 'Pout [Pa]', 'Delta P [Pa]',
       'M [kg/s]', 'Torque [N-m]', 'Tin [^oC]',
       'Tout [^oC]', 'Omega [W]', 'eta [-]',
       'd Pin /dt', 'dPout /dt',
       'd Delta P /dt', 'd M /dt',
       'Torque /dt', 'd Tin /dt',
       'd Tout /dt', 'd \Omega /dt', 'd \eta /dt']
target_names = ['CV','DIP','VI', 'GI']
export_graphviz(tree_c, out_file= savefolder+ 'Tree.dot', 
            class_names=target_names, feature_names=feature_names, 
            impurity=True, filled=True, node_ids=True, 
            rounded=True, special_characters=True, 
            #rotate=True
            )
graphviz.render('dot','pdf',savefolder + 'Tree.dot')


#%%#########################################################################
# Calculate Table 4 Experimets, Validation and Test Distribution
############################################################################
F = [0 , 1, 3, 5, 7]
G = [0 , 1, 2, 3, 4, 5]
Count_Exp = np.empty((len(F),len(G)))
for f1 in range(len(F)):
    for g1 in range(len(G)):
        f = F[f1]
        g = G[g1]
        Count_Exp[f1,g1] = np.intersect1d(
            Index['Bi2'][f], G_Index[g]).shape[0]

#%%#########################################################################
# Labeling of Choke Valve 
############################################################################
folderfile = '0_Data/2_Samples/'
file = 'T50_RPM3500_2KGBEP_VD9-C'
Sample = np.load(folderfile + file + '.npy')
sample = fn.Sample_lvm(Sample)
W = 33;     t = sample.t;   t_s = sample.t_s        # Windows and Time
P1 = fn.mean_movil(sample.P1,W)*1E-3                # Pressure [Bar]
P2 = fn.mean_movil(sample.P2,W)*1E-3                # Pressure [Bar]
M = fn.mean_movil(sample.M,W)                       # Mass Flow [kg/s]
Mgas = fn.mean_movil(sample.Mgas,W)*3600            # Gas Mas Flow [kg/h]
Var1 = lambda X: [X[j:j+W].var() for j in range(0,len(X))]
Var2 = lambda X: [X[j:].var() for j in range(0,len(X))]
P1var1 = Var1(P1);    P2var1 = Var1(P2)             # Pressure Variance
Mvar1 = Var1(M)                                     # Mass Variance
P1var2 = Var2(P1);    P2var2 = Var2(P2)             # Pressure Variance
Mvar2 = Var2(M)                                     # Mass Variance
# Plot Comparation between P1, P2 and M ####################################
x = [P1,P2/100,M];  x_s = ['$p_{in} [kPa]$','$p_{out} [bar]$','$\dot{m} [kg/h]$']
x_c = [(0.1,0.1,0.1), (0.7,0.7, 0.7), (0.5,0.5,0.5)]
x_t =[':o', '--o', '-']
s = [sample.Choke, sample.State*sample.Choke.max()*1.1]
s_s = [ 'CV [%]', 'Window']
s_c = [(0,0.2,0),(0.7,0,0)]
Yt = [11,12]; Yl = [10.9, 12.1]   # Define Secondary ticks and Limits
plt.clf(); plt.cla(); plt.close()
Fig5 = fn.Plot(t, t_s, x, x_s, x_c, x_t, s, s_s, s_c,'Diff',Yt,Yl,
    fontsize=25,labelsize=30,labelrotation=90.,linewidth=5,
    markersize=12, markerevery=102)

plt.savefig(savefolder + 'labchoke4.svg')
